# Guide d'utilisation

## Tableau de bord

Une fois l'application lancée. Le tableau de bord s'affiche en premier. Cet écran présente une vue d'ensemble des différents outils disponnibles. Il est alors facile d'activer ou de désactiver les fonctionnalités désirées.

De base, 4 modules sont désactivés. Il suffit d'activer un à un grâce au petit curseur gris qui deviendra vert une fois activé.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_tdb.png?ref_type=heads)


## Utilisation de base 

Pour découvrir un module, il suffit de cliquer sur ce derneir dans le menu déroulant à gauche.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_exemple.png?ref_type=heads)

Les modules PowerToys sont très faciles d'utilisation. CHacun d'entre eux est composé de la même manière :

- Une section avec les informations de base tels que le titre, une image et une description brève du module.
- Une petite section avec la possibilité d'activer ou de désactiver le module.
- Une section de paramétrage. Tout module possède un raccourci d'activation modifiable ainsi que des otpions qui changent en fonction de la fonctionnalité.



possibilité barre tache


## Général

Dans cet onglet, l'utilisateur peut modifier certains réglages.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_general.png?ref_type=heads)

### Version et mises à jour
Une première division permet de vérifier si des mises à jour sont disponible. Il suffit alors de cliquer sur le bouton Rechercher les mises à jour. Il permettra ensuite le téléchargement et l'installation des mises à jour. Si PowerToys est à jour, il l'affichera juste en dessous.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_maj.png?ref_type=heads)

En développant la section deux nouvelles options sont disponibles: 

- Une case à cocher permet de faire notifier le logiciel lorsqu'une nouvelle version est disponible.
- Une autre permet l'affichage des notes de version après mise à jour.

### Mode Administrateur

Exécuter PowerToys en mode administrateur peut être nécessaire dans certaines situations pour permettre à certaines fonctionnalités d'accéder et de modifier des paramètres système ou des fichiers nécessitant des privilèges élevés. 
Par défaut, le logiciel affiche une notification lorsque le mode administrateur est nécessaire.
Il est alors possible de redémarrer PowerToys en administrateur grace au bouton Redémarrer PowerToys en tant qu'administrateur.
La case Toujours exécuter comme administrateur est grisée et devriendra cochable une fois passé en mode administrateur. Coomme son nom l'indique, chaque fois que PowerToys sera lancé, il le sera avec les privilèges administrateur.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_admin.png?ref_type=heads)

### Apparence et comportement

Le thème de l'application est modifiable. Par défaut il suit le thème de Windows. Grâce au menu déroulant, il est possible de forcer le mode sombre ou le mode clair.

Un curseur est disponible afin d'exécuter PowerToys au lancement de Windows afin de le rendre immédiatement opérationnel.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_apparence.png?ref_type=heads)

### Sauvegarde et restauration

Une fois le logiciel configuré, il est possible de sauvegarder les paramètres. En cliquant sur Sauvegarder, une sauvegarde sera effectué dans un fichier json : "C:\Users\{nomUtilisateur}\AppData\Local\Microsoft\PowerToys\settings.json"
Il est alors possible d'effectuer des test de configuration tout en ayant la possibilité de retourner à une la sauvegarde précédente en cliquant sur Restaurer.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_sauvegarde.png?ref_type=heads)

### Expérimentation

Pour les personnes inscrites au programme Insider de Microsoft, il est possible d'activer les fonctionnalités mise en expérimentation par l'équipe de développement. 

Les participants au programme, appelés "Windows Insiders", bénéficient d'un accès anticipé aux nouvelles fonctionnalités, mises à jour et améliorations. Ils peuvent fournir des retours à Microsoft pour contribuer à améliorer la qualité du logiciel avant sa sortie officielle.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/utilisation_experimentation.png?ref_type=heads)
