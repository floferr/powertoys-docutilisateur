# Awake

Permet de maintenir actif son ordinateur sans modifier les options d'alimentation ou autre. Grâce à cet outil, il est facile d'activer et de désactiver ponctuellement la mise en veille. Il est possible de paramétrer un compte à rebour pour retourner à l'état initial (actif ou inactif).

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/awake_comportement.png?ref_type=heads)

Le menu déroulant de l'option Mode, permet de chosiir parmi quatre choix :

- Continuer à utiliser le mode de gestion de l'alimentation selectionné (L'application ne modifie rien, il agit comme s'il n'était pas activé).
- Maintenir actif indéfiniment (L'ordinateur reste éveillé jusqu'à désactivation de cette option).
- Rester actif pendant un intervalle de temps (L'ordinateur reste éveillé pendant le temps défini par l'utilisateur, ensuite revient au mode précedemment utilisé).
- Rester actif jusqu'à expiration (L'ordinateur reste éveillé jusqu'à la date et l'heure sélectionné par l'utilisateur).

En activant l'option Maintenir l'écran activé, l'écran externe connecté au poste restera allumé pendant la durée de l'éveille.