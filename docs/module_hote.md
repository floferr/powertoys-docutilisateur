# Éditeur de fichiers hôtes

Permet l'édition facile des fichiers hôtes de Windows. Pour rappel, le fichier hôte joue le rôle de DNS local qui est éxécuté avant tous les autres DNS.