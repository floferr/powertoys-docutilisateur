# Toujours visible

Permet d'épingler une fenêtre sur le devant de votre écran. Cette fenêtre sera alors au dessus de tout autre application.

Il suffit de sélectionner une fenêtre et de presser les touches Windows + Ctrl + T afin de l'épingler.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/toujoursVisible_demo.gif?ref_type=heads)

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/toujoursVisible_activation.png?ref_type=heads)

Il est possible de désactiver ce module lorsque le mode jeu est activé pour plus d'ergonomie.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/toujoursVisible_apparence.png?ref_type=heads)

Ses options permettent de modifier l'affichage de la fenêtre épinglé afin de repérer plus facilement lorsque le module est actif ou non. Il est possible de choisir la couleur, l'opacité et l'épaisseur du contour de mise en évidence.

Une case à cocher est disponible afin de jouer une notification lors de l'activation de la fonctionnalité.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/toujoursVisible_exclusion.png?ref_type=heads)

Une zone de texte permet facielement d'empêcher l'exécution du module sur certaines fenêtre. Il suffit alors d'entrer le nom de l'application comme par exemple : firefox.exe.