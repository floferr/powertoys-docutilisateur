# Bienvenu dans la documentation utilisateur de PowerToys !

Inspiré par le projet PowerToys de l'ère Windows 95, cette refonte offre aux utilisateurs expérimentés des moyens d'exploiter davantage l'efficacité du shell Windows 10 et de le personnaliser pour répondre à des flux de travail individuels.

PowerToys sont des outils développés par Microsoft. Parfois complexe pour des utilisateurs lambda. Les développeurs ont décidé de publier ces fonctionnalités avancées trés utiles dans un logiciel venant à part. Ainsi les utilisateurs aguéris pourront bénéficier de cet assemblage de fonctionnalité.


<div class="grid cards" markdown>

- **Aperçu**

    ---
    - [Informations](informations.md)
    - [Introduction](introduction.md)
    - [Résumé](resume.md)

- **Guides**

    ---
    - [Guide d'installation](installation.md)
    - [Guide d'utilisation](utilisation.md)
    - [Guide de désinstallation](desinstallation.md)

- **Modules**

    ---
    - [Aperçu](module_apercu.md)
    - [Awake](module_awake.md)
    - [Bordure](module_bordure.md)
    - [Coller Texte Brute](module_collerBrut.md)
    - [Color Picker](module_colorPicker.md)
    - [Extracteur de texte](module_extracteur.md)
    - [FancyZones](module_fancyZones.md)
    - [Explorateur de fichiers](module_fichier.md)
    - [Éditeur de fichiers hôtes](module_hote.md)
    - [Image Resizer](module_imageResizer.md)
    - [Keyboard Manager](module_keyboard.md)
    - [Fichier Locksmith](module_locksmith.md)
    - [PowerRename](module_powerRename.md)
    - [PowerToys Run](module_powertoysRun.md)
    - [Aperçu du registre](module_registre.md)
    - [Réticules du pointeur de souris](module_reticule.md)
    - [Rogner et vérouiller](module_rogner.md)
    - [Saut de souris](module_saut.md)
    - [Shortcut guide](module_shortcut.md)
    - [Surligneur de souris](module_surligneur.md)
    - [Toujours visible](module_toujourVisible.md)
    - [Trouver ma souris](module_trouverSouris.md)
    - [Variables d'environnement](module_variablesEnvironnement.md)
</div>

---

- [Bugs Connus](bugs.md)
- [Assistance et liens utiles](assistance.md)




