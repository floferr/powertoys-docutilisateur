# Fichier Locksmith

Permet de connaître quels processus vérouillent le fichier.

Exemple : Il arrive parfois qu'un fichier n'est pas supprimable car il est en cours d'utilisation. Avec Fichier Locksmith, il sera possible de détecter ce qui bloque.