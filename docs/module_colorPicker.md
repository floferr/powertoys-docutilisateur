# Color Picker

Permet d'ajouter une capture de couleur au pointeur de la souris. En cliquant gauche, le code de la couleur pointée est affiché sous les formats prédéfini au préalable.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/colorPicker_raccourci.png?ref_type=heads)

Pour utiliser le module, il suffit de presser Windows + Shift + C. 

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/colorPicker_demo.gif?ref_type=heads)

Il est possible de modifier le comportement de ce dernier lors de l'appui de la combinaison de touches. Grâce au menu déroulant, trois choix sont disponibles :

- Ouvrir l'éditeur (Ouvre une fenêtre de sélecteur de couleurs)
- Choisir une couleur et ouvrir l'éditeur  (Transforme le curseur en pipette, copie la couleur dans le presse papier lors du clic gauche et ouvre la fenêtre de sélecteur de couleurs)
- Sélectionner une couleur (Transforme le curseur en pipette et copie la couleur dans le presse papier lors du clic gauche)

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/colorPicker_comportement.png?ref_type=heads)

L'affichage de la couleur en mode sélection de couleur est modifiable. Le menu déroulant propose une panoplie de formats différents tels que HEX, RGB, HSL, CMYK, HSB, HSI, HWB, NCol, CIELAB, CIEXYZ, VEC4 ou Decimal.
Un curseur est dispoonible pour ajouter ou enlever l'affichage du nom de la courleur pointée.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/colorPicker_format.png?ref_type=heads)

Il est possible d'ajouter des formats personnalisés en cliquant sur Ajouter un nouveau format. Une fenêtre de paramétrage s'ouvre alors.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/colorPicker_ajoutFormat.png?ref_type=heads)