# Assistance / Liens utiles

- Visiter Microsoft Learn : https://learn.microsoft.com/en-us/windows/powertoys/
- Contribuer dans le projet : https://github.com/microsoft/PowerToys
- Signaler une faille de sécurité : https://aka.ms/security.md/msrc/create-report ou secure@microsoft.com.
- Demande de fonctionnalités : https://github.com/microsoft/PowerToys/issues/new?assignees=&labels=Triage-Needed&template=feature_request.yml
