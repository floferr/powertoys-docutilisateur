# PowerRename

Permet le renommage de fichier en masse. De même qu'Image Resizer, il est très facile de changer le nom des fichiers avec un outil complet avec de nombreuses options. Il est possible d'utiliser des expressions régulières.