# Informations

| Nom du projet | Microsoft PowerToys |
|--|--|
| Date | 11 Avril 2024 |
| Version | 0.80.1 |
| Mots-clés | #Windows, #powertoys, #microsoft-powertoys |
| Auteur | Microsoft |