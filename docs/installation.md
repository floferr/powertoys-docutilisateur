# Guide d'installation

L'installation peut se faire de différentes manières. Il existe deux manières conseillées afin de l'installer. Soit par GitHub, soit par le store de Microsoft. L'installation par la deuxième option est plus facile mais see peut qu'il ne soit pas à jour dépendamment de votre système.

**Prérequis**
Il est nécessaire d'avoir un système d'exploitation Windows 10 ou Windows 11.

## Installation avec GitHub

1. Accéder au repository grâce au lien ci-contre : https://github.com/microsoft/PowerToys
2. Faire défiler la page jusqu'à trouver Downloads & Release notes
3. S'assurer que c'est bien la dernière version du logiciel.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/installation_latest.png?ref_type=heads)

4. Cliquer sur le lien à côté de Per user - x64. Ce lien devrait être de la forme PowerToysSetup-0.##.#-x64.exe.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/installation_github.gif?ref_type=heads)

5. Attendre que le téléchargement finisse et procéder à l'installation.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/installation_exe.gif?ref_type=heads)

## Installation avec Microsoft Store

1. Ouvrir le Microsoft Store.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/installation_ouverture_store.png?ref_type=heads)

2. Rechercher PowerToys dans la barre de recherche.
3. Cliquer sur Installer.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/pt_installation_store.gif?ref_type=heads)

## Installation par gestionnaire de paquet

1. Installer le gestionnaire de paquet Chocolateyr en suivant les étapes du lien suivant : https://chocolatey.org/install

2. Exécuter la commande suivante dans un invite de commande: choco install powertoys -y

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/installation_chocolatey.gif?ref_type=heads)

# Après installation

Une fois l'installation effectuée. Tous les outils de l'application sont accessibles.
Une fois exécutée, une fenêtre icone se place dans la barre des tâches et montre des options réduites et facilement accessibles.