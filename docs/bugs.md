# Bugs connus

Tous les rapports de bug sont disponnibles sur le GitHub de PowerToys : https://github.com/microsoft/PowerToys/issues

## Sécurité

Microsoft privilégie la sécurité de ses utilisateurs et souhaite limiter la divulgation de faille importante pouvant mettre en danger les données de personnes tiers. Pour cette raison, les rapports de faille de sécurité se font par mail chiffré par clé PGP à l'adresse : secure@microsoft.com ou alors sur le site https://aka.ms/security.md/msrc/create-report.