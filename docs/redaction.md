# Rédaction et modifications

| Version | Date | Nom | Description |
|--|--|--|--|
| 1.0 | 06/05/2024 | Florian FERRERE | Première version, plan principal. |
| 2.0 | 07/05/2024 | Florian FERRERE | Amélioration de l'affichage, remplissage des différents guides. |