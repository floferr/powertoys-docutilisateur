# 2 - Introduction

Les PowerToys sont une collection d'outils logiciels développés par Microsoft pour améliorer la productivité et l'expérience utilisateur sur les systèmes Windows.

Ils proposent des fonctionnalités qui améliorent la productivité et ajoutent de la customisation au système Windows.

La première version de PowerToys remonte à Windows 95. PowerToys pour Windows XP fut la seconde version de PowerToys. Depuis le 8 Mai 2019, Microsoft a mis disponible PowerToys pour Windows 10 sur GitHub. Il devient alors open-source. Il est également disponible sous Windows 11.