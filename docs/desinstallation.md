# Guide de désinstallation

## Désinstallation à l'aide graphique à l'aide du gestionnaire d'applications

Ouvrir les paramètres de votre système Windows. Chercher Applications et fonctionnalités.
Une fois le gestionnaire ouvert, taper "powertoys" dans le champ de recherche. Le logiciel devrait apparaître dans la liste centrale. Cliquer ensuite sur la zone Powertoys afin d'étendre les options. Cliquer simplement sur Désinstaller afin de procéder à la désintallation. 

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/desinstallation_applications.png?ref_type=heads)

## Déinstallation à l'aide du gestionnaire de paquet Chocolatey

Pour désinstaller l'application grâce à chocolatey, il suffit d'exécuter la commande suivante dans un invite de commande : choco uninstall powertoys -y.

![](https://gitlab.com/floferr/powertoys-docutilisateur/-/raw/main/img/desinstallation_chocolatey.gif?ref_type=heads)

